<?php

require_once ("../connection.php");

session_start();

if (isset($_POST['submit'])) {
    $db = connect_to_database();

    $ssn = $_POST["ssn"];
    $fname = $_POST["fname"];
    $lname = $_POST["lname"];
    $age = $_POST["age"];
    $salary = $_POST["salary"];
    $gender = $_POST['gender'];
    $role = $_POST["role"];
    $city = $_POST["city"];
    $area = $_POST["area"];
    $addr = $_POST["address"];
    $post_code = $_POST["post-code"];
    $phone = $_POST["phone-no"];
    echo $phone.'\n';
    $date = date("Y");

    $q = "INSERT INTO `person` (national_code, fname, lname, gender, city, area, address, post_code, phone_no, Eflag, salary, age, start_year, role) 
          VALUES 
          ('$ssn', '$fname', '$lname', '$gender', '$city', '$area', '$addr', '$post_code', '$phone', '1', '$salary', '$age', '$date', '$role')";

    $res = mysqli_query($db, $q);
    echo $res;

    disconnect_from_database($db);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>مدیریت کارمندان</title>
    <link rel="stylesheet" type="text/css" href="../css/foundation.min.css">
    <link rel="stylesheet" type="text/css" href="../css/app.css">
    <link rel="stylesheet" type="text/css" href="../css/Forms.css">
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">-->
</head>
<body>

<div class="header">
    <div class="topbar">
        <div class="topbar-right">
            <span>سلام، <?php echo $_SESSION['admin_email']; ?></span>
            <span><a href="Admin-login.php" class="button error">خروج</a></span>
        </div>
        <div class="topbar-left">
            <img class="logo" src="../img/logo.png">
        </div>
    </div>
    <div>
        <ul class="menu">
            <a href="Admin-employee.php"><li>مدیریت کارمندان</li></a>
            <a href="Admin-warehouse.php"><li>مدیریت انبارها</li></a>
            <a href="admin_product.php"><li>مدیریت کالاها</li></a>
            <a href="Admin-discount.php"><li>مدیریت تخفیف‌ها</li></a>

        </ul>
    </div>
</div>

<div class="container" >
    <h3 class="form-title">کارمند جدید</h3>
    <hr style="color: #0a0a0a">
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">نام</label></div>
            <input type="text" class="form-normal-input" name="fname">
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">نام‌خانوادگی</label></div>
            <input type="text" class="form-normal-input" name="lname">
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">کد ملی</label></div>
            <input type="text" class="form-normal-input" name="ssn">
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">سن</label></div>
            <input type="number" class="form-normal-input" name="age">
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">حقوق‌ماهیانه</label></div>
            <input type="number" class="form-normal-input" name="salary">
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">جنسیت</label></div>
            <select style="width: 100px" name="gender">
                <option value="1">مرد</option>
                <option value="0">زن</option>
            </select>
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">سمت</label></div>
            <select style="width: 150px" name="role">
                <option value="warehouse">مسئول انبار</option>
                <option value="seller">مسئول فروش</option>
                <option value="deliver">مسئول مرسولات</option>
            </select>
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">شهر</label></div>
            <input type="text" class="form-normal-input" name="city">
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">منطقه</label></div>
            <input type="text" class="form-normal-input" name="area">
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">کدپستی</label></div>
            <input type="text" class="form-normal-input" name="post-code">
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">آدرس</label></div>
            <textarea class="form-textarea" name="address"></textarea>
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">شماره تلفن</label></div>
            <input type="text" class="form-normal-input" name="phone-no">
        </div>

        <br>

        <div class="form-row">
            <input type="submit" name="submit" style="float: left" class="button success" value="ثبت اطلاعات">
        </div>

        <div class="form-row">
            <br>
        </div>
    </form>
</div>

<div>
    <br><br>
</div>

</body>
</html>