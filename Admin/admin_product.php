<?php

require_once ("../connection.php");

session_start();

$db = connect_to_database();


if (isset($_POST['submit']) || isset($_POST['submit_new_color'])) {
    $name = $_POST["name"];
    $producer = $_POST["producer"];
    $substance = $_POST["substance"];
    $price = $_POST["price"];
    $image = $_POST["image"];
    $description = $_POST["description"];

    if (isset($_POST['submit_new_color'])) {
        $color = $_POST["new_color"];
        $new_color_q = "INSERT INTO `color` (cvalue) VALUES ('$color')";
        $db->query($new_color_q);
    } else if (isset($_POST['submit'])) {
        $product_type = $_POST["product_type"];

        switch ($product_type) {
            case "mobile":
                $mobile_sim = $_POST["mobile_sim"];
                $mobile_cpu = $_POST["mobile_cpu"];
                $mobile_ram = $_POST["mobile_ram"];
                $mobile_mem = $_POST["mobile_storage"];
                $mobile_dis = $_POST["mobile_display"];
                $mobile_cam = $_POST["mobile_camera"];
                $mobile_os = $_POST["mobile_os"];

                $mobile_q = "INSERT INTO `mobile` (display, cpu, ram, camera, os, memory, sim)
                              VALUES
                              ('$mobile_dis', '$mobile_cpu', '$mobile_ram', '$mobile_cam', '$mobile_os', '$mobile_mem', '$mobile_sim')";
                if ($db->query($mobile_q) === TRUE) {
                    $mobile_id = $db->insert_id;

                    $new_product_q = "INSERT INTO `product` (producer, name, description, price, image, substance, mobile_id, category_type)
                                      VALUES 
                                      ('$producer', '$name', '$description', '$price', '$image', '$substance', '$mobile_id', '$product_type')";
                    if ($db->query($new_product_q) === TRUE) {
                        $product_id = $db->insert_id;
                    }
                }


                break;
            case "laptop":
                break;
            default:
                break;
        }

        $select_colors = $_POST["select_colors"];
        if (!empty($select_colors)) {
            $n = count($select_colors);
            for ($i = 0; $i < $n; $i++) {
                $db->query("INSERT INTO `product_color` (cid, pid) VALUE ('$select_colors[$i]', '$product_id')");
            }
        }

        $warr_company = $_POST["warranty_company"];
        $warr_duration = $_POST["warranty_duration"];
        if (!empty($warr_company) && !empty($warr_duration)) {
            $year = date("Y");
            $db->query("INSERT INTO `warranty` (pid, company. duration, start_date) VALUES ('$product_id', '$warr_company', '$warr_duration', '$year')");
        }
    }

} else if (isset($_POST["submit_amount"])) {
    $pid = $_POST["pid"];
    $amount = $_POST["amount"];
    $warehouse = $_POST["warehouse"];

    $get_current_amount_q = "SELECT count FROM `exist` WHERE pid = '$pid' and wid = '$warehouse'";
    $res = $db->query($get_current_amount_q);
    if (mysqli_num_rows($res) == 1) {
        $row = $stuffs_sql_array->fetch_assoc();
        $current_val = $row["count"];
        $current_val += $amount;
        $db->query("UPDATE `exist` SET count = '$current_val' WHERE pid = '$pid' and wid = '$wid'");
    } else if (mysqli_num_rows($res) == 0) {
        $db->query("INSERT INTO `exist` (pid, wid, count) VALUES ('$pid', '$warehouse', '$amount')");
    }
}

$get_colors_q = "SELECT * FROM `color`";
$colors = $db->query($get_colors_q);

$get_stuffs_q = "SELECT pid, producer, name FROM product";
$stuffs_sql_array = $db->query($get_stuffs_q);

$get_warehouses_q = "SELECT name FROM warehouse";
$warehouses_sql_array = $db->query($get_warehouses_q);

disconnect_from_database($db);

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>مدیریت کالاها</title>
        <link rel="stylesheet" type="text/css" href="../css/foundation.min.css">
        <link rel="stylesheet" type="text/css" href="../css/app.css">
        <link rel="stylesheet" type="text/css" href="../css/Forms.css">
        <link rel="stylesheet" type="text/css" href="css/Admin-product.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <script src="../js/vendor/jquery.js" type="text/javascript"></script>
    </head>
    <body>

        <div class="header">
            <div class="topbar">
                <div class="topbar-right">
                    <span>سلام، <?php echo $_SESSION['admin_email']; ?></span>
                    <span><a href="Admin-login.php" class="button error">خروج</a></span>
                </div>
                <div class="topbar-left">
                    <img class="logo" src="../img/logo.png">
                </div>
            </div>
            <div>
                <ul class="menu">
                    <a href="Admin-employee.php"><li>مدیریت کارمندان</li></a>
                    <a href="Admin-warehouse.php"><li>مدیریت انبارها</li></a>
                    <a href="admin_product.php"><li>مدیریت کالاها</li></a>
                    <a href="Admin-discount.php"><li>مدیریت تخفیف‌ها</li></a>

                </ul>
            </div>
        </div>
        
        <div class="container" >
            <h3 class="form-title">کالای جدید</h3>
            <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <div class="form-row">
                    <div class="product-form-labels-col"><label class="form-label">نام</label></div>
                    <input type="text" class="form-normal-input" name="name" value="<?php if(isset($name)) echo $name ?>" required>
                </div>
                <div class="form-row">
                    <div class="product-form-labels-col"><label class="form-label">تولیدکننده</label></div>
                    <input type="text" class="form-normal-input" name="producer" value="<?php if(isset($producer)) echo $producer ?>" required>
                </div>
                <div class="form-row">
                    <div class="product-form-labels-col"><label class="form-label">جنس</label></div>
                    <input type="text" class="form-normal-input" name="substance" value="<?php if(isset($substance)) echo $substance ?>" required>
                </div>
                <div class="form-row">
                    <div class="product-form-labels-col"><label class="form-label">قیمت</label></div>
                    <input type="number" class="form-normal-input" name="price" value="<?php if(isset($price)) echo $price ?>" required>
                </div>
                <div class="form-row">
                    <div class="product-form-labels-col"><label class="form-label">تصویر</label></div>
                    <input type="file" class="form-normal-input" accept=".jpg,.jpeg,.png" name="image"  value="<?php if(isset($image))  echo $image ?>" required>
                </div>
                <div class="form-row">
                    <div class="product-form-labels-col"><label class="form-label">شرح کالا</label></div>
                    <textarea class="form-textarea" name="description" required><?php if(isset($description)) echo $description ?></textarea>
                </div>
                <div class="form-row">
                    <div class="product-form-labels-col"><label class="form-label">رنگ ها</label></div>
                    <div id="colors">
                        <?php
                        if ($colors->num_rows > 0) {
                            while ($row = $colors->fetch_assoc())
                                echo '<span class="tuple">'.$row["cvalue"].'<input type="checkbox" class="colorcheck" name="select_colors[]" value="'.$row["cid"].'"></span>';
                        }
                        ?>
                    </div>
                    <br>
                </div>
                <div class="form-new-l2-label">
                    <div>
                        <div class="product-form-labels-col"><label class="form-label">رنگ جدید</label></div>
                        <input type="text" class="form-new-input" id="new-color" name="new_color"  maxlength="15" required>
                        <input type="submit" class="button primary" id="add-color" value="افزودن" name="submit_new_color">
                    </div>
                </div>
                <div class="form-row" id="warranty-row">
                    <div class="product-form-labels-col"><label class="form-label">گارانتی</label></div>
                </div>
                <br>
                <div class="form-new-l2-label">
                    <div>
                        <div class="product-form-labels-col"><label class="form-label">شرکت</label></div>
                        <input type="text" class="form-new-input" id="warranty-company" name="warranty_company"  maxlength="15">
                        <br>
                        <div class="product-form-labels-col"><label class="form-label">مدت</label></div>
                        <input type="number" name="warranty_duration" class="form-new-input" id="warranty-period" maxlength="15">
                        <input type="button" name="submit_new_warranty" class="button primary" id="add-color" value="افزودن">
                    </div>
                </div>
                <div class="form-row">
                    <div class="product-form-labels-col"><label class="form-label">دسته کالا</label></div>
                    <select style="width: 150px" onchange="typeChanged(this.value)" name="product_type">
                        <option value="mobile">موبایل</option>
                        <option value="laptop">لپ تاپ</option>
                        <option value="camera">دوربین</option>
                    </select>
                </div>

                <div id="mobile-section">
                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">تعداد سیم کارت</label></div>
                        <input type="number" min="1" max="3" name="mobile_sim" class="form-normal-input">
                    </div>
                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">پردازنده</label></div>
                        <input type="text" name="mobile_cpu" class="form-normal-input">
                    </div>
                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">حافظه داخلی</label></div>
                        <input type="text" name="mobile_storage" class="form-normal-input">
                    </div>

                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">حافظه RAM</label></div>
                        <input type="text" name="mobile_ram" class="form-normal-input">
                    </div>
                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">صفحه نمایش</label></div>
                        <input type="text" name="mobile_display" class="form-normal-input">
                    </div>
                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">دوربین</label></div>
                        <input type="text" name="mobile_camera" class="form-normal-input">
                    </div>
                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">سیستم عامل</label></div>
                        <input type="text" name="mobile_os" class="form-normal-input">
                    </div>
                    <br>
                </div>

                <div id="laptop-section" style="display: none;">
                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">پردازنده</label></div>
                        <input type="text" name="laptop_cpu" class="form-normal-input" >
                    </div>
                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">حافظه داخلی</label></div>
                        <input type="text" name="laptop_storage" class="form-normal-input" >
                    </div>

                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">RAM</label></div>
                        <input type="text" name="laptop_ram" class="form-normal-input" >
                    </div>
                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">صفحه نمایش</label></div>
                        <input type="text" name="laptop_display" class="form-normal-input" >
                    </div>
                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">گرافیک</label></div>
                        <input type="text" name="laptop_graphic" class="form-normal-input" >
                    </div>
                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">سیستم عامل</label></div>
                        <input type="text" name="laptop_os" class="form-normal-input" >
                    </div>
                    <br>
                </div>

                <div id="camera-section" style="display: none;">
                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">وزن</label></div>
                        <input type="number" name="camera_weight" class="form-normal-input" >
                    </div>
                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">کیفیت عکاسی</label></div>
                        <input type="text" name="camera_image_quality" class="form-normal-input" >
                    </div>

                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">کیفیت فیلم</label></div>
                        <input type="text" name="camera_film_quality" class="form-normal-input" >
                    </div>
                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">صفحه نمایش</label></div>
                        <input type="text" name="camera_display" class="form-normal-input" >
                    </div>
                    <div class="form-row">
                        <div class="product-form-labels-col"><label class="form-label">زوم اپتیکال</label></div>
                        <input type="text" name="camera_zoom" class="form-normal-input" >
                    </div>
                    <br>
                </div>

                <div class="form-row">
                    <input type="submit" name="submit" style="float: left" class="button success" value="ثبت اطلاعات">
                </div>

            </form>

            <div class="form-row">
                <br>
            </div>

            <div class="form-row">
                <br>
            </div>
            <hr style="color: #020202">
            <div class="form-row">
                <br>
            </div>
            <h3 class="form-title">افزودن تعداد کالا</h3>
            <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <div class="form-row">
                    <div class="product-form-labels-col"><label class="form-label">انتخاب کالا</label></div>
                    <select style="width: 150px" onchange="typeChanged(this.value)" name="pid">
                        <?php
                        if ($stuffs_sql_array->num_rows > 0) {
                            while ($row = $stuffs_sql_array->fetch_assoc())
                                echo '<option value="'.$row["pid"].'">'.$row["producer"].' - '.$row["name"].'</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="form-row">
                    <div class="product-form-labels-col"><label class="form-label" id="l1">تعداد</label></div>
                    <input type="number" class="form-normal-input" name="amount" required>
                </div>
                <div class="form-row">
                    <div class="product-form-labels-col"><label class="form-label">انبار</label></div>
                    <select style="width: 150px" onchange="typeChanged(this.value)" name="warehouse">
                        <?php
                        if ($warehouses_sql_array->num_rows > 0) {
                            while ($row = $warehouses_sql_array->fetch_assoc())
                                echo '<option value="'.$row["wid"].'">'.$row["name"].'</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="form-row">
                    <input type="submit" name="submit_amount" style="float: left" class="button success" value="ثبت اطلاعات">
                </div>
            </form>
            <div class="form-row">
                <br>
            </div>
            <div class="form-row">
                <br>
            </div>
            
            
            
            
        </div>
        <script src="../js/admin-product.js"></script>
        <br>
        <br>
    </body>
</html>