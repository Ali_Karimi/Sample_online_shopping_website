<?php

require_once ("../connection.php");

if (isset($_POST['submit'])) {
    $db = connect_to_database();
    $user = $_POST['admin-email'];
    $pass = $_POST['admin-pass'];

    echo $user.'\n';
    echo $pass;

    if (!empty($user) && !empty($pass)) {
        $q = "SELECT * FROM `admin` WHERE email ='$user' and password ='$pass'";
        $data = mysqli_query($db, $q);
        if (mysqli_num_rows($data) != 0) {

            // set session
            session_start();
            $row = mysqli_fetch_array($data);
            $admin_id = $row['id'];
            $_SESSION['admin_id'] = $admin_id;
            $_SESSION['admin_email'] = $user;

            // redirect
            $admin_home = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/Admin-employee.php';
            header('Location: ' . $admin_home);
        }
    }

    disconnect_from_database($db);
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ورود مدیر</title>
        <link href="../bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../bootstrap-3.3.6-dist/css/bootstrap-theme.css" rel="stylesheet">
        <link href="css/Admin-login.css" rel="stylesheet">
        <link href="../css/app.css" rel="stylesheet">
        <script type="text/javascript" src="../bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
    </head>
    <body>

        <div class="container">

            <div class="page-header">
                <h1>پنل مدیریت فروشگاه</h1>
            </div>

            <form class="form-signin" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                    <h2 class="form-signin-heading">لطفا وارد شوید</h2>
                    <label for="inputEmail" class="sr-only">نام کاربری</label>
                    <input type="email" name="admin-email" id="inputEmail" class="form-control" placeholder="نام کاربری" required autofocus>
                    <label for="inputPassword" class="sr-only">رمز عبور</label>
                    <input type="password" name="admin-pass" id="inputPassword" class="form-control" placeholder="رمز عبور" required>
                    <input value="ورود" class="btn btn-lg btn-primary btn-block" type="submit" name="submit">
            </form>
        </div>
        <script src="../js/admin-login.js"></script>
    </body>
</html>