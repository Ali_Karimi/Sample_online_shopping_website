<?php

require_once ('../connection.php');

session_start();

$db = connect_to_database();

$warehouse_emps_q = "SELECT * FROM `person` WHERE role = 'warehouse'";
$warehouse_emps = $db->query($warehouse_emps_q);

if (isset($_POST["submit"])) {
    $name = $_POST["wname"];
    $cap = $_POST["capacity"];
    $resp = $_POST["responsible"];
    $city = $_POST["city"];
    $area = $_POST["area"];
    $post = $_POST["post_code"];
    $addr = $_POST["address"];
    $phone = $_POST["phone_no"];

    $new_warehouse_q = "INSERT INTO `warehouse` 
                        (name, capacity, city, area, address, post_code, phone_no)
                        VALUES
                        ('$name', '$cap', '$city', '$area', '$addr', '$post', '$phone')";
    if ($db->query($new_warehouse_q) === TRUE) {
        $last_id = $db->insert_id;

        $update_person_wid_q = "UPDATE person SET wid = '$last_id' WHERE national_code = '$resp'";
        if ($db->query($update_person_wid_q) === TRUE) {
//            echo "success";
        }
    }
}


disconnect_from_database($db);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>مدیریت انبار</title>
    <link rel="stylesheet" type="text/css" href="../css/foundation.min.css">
    <link rel="stylesheet" type="text/css" href="../css/app.css">
    <link rel="stylesheet" type="text/css" href="../css/Forms.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>

<div class="header">
    <div class="topbar">
        <div class="topbar-right">
            <span>سلام، <?php echo $_SESSION['admin_email']; ?></span>
            <span><a href="Admin-login.php" class="button error">خروج</a></span>
        </div>
        <div class="topbar-left">
            <img class="logo" src="../img/logo.png">
        </div>
    </div>
    <div>
        <ul class="menu">
            <a href="Admin-employee.php"><li>مدیریت کارمندان</li></a>
            <a href="Admin-warehouse.php"><li>مدیریت انبارها</li></a>
            <a href="admin_product.php"><li>مدیریت کالاها</li></a>
            <a href="Admin-discount.php"><li>مدیریت تخفیف‌ها</li></a>

        </ul>
    </div>
</div>

<div class="container" >
    <h3 class="form-title">انبار جدید</h3>
    <hr style="color: #0a0a0a">
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">نام انبار</label></div>
            <input type="text" class="form-normal-input" name="wname">
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">ظرفیت</label></div>
            <input type="number" class="form-normal-input" name="capacity">
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">مسئول</label></div>
            <select style="width: 260px" name="responsible">
                <?php
                if ($warehouse_emps->num_rows > 0) {
                    while ($row = $warehouse_emps->fetch_assoc())
                        echo '<option value="'.$row["national_code"].'">'.$row["fname"].' '.$row["lname"].'</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">شهر</label></div>
            <input type="text" class="form-normal-input" name="city">
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">منطقه</label></div>
            <input type="text" class="form-normal-input" name="area">
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">کدپستی</label></div>
            <input type="text" class="form-normal-input" name="post_code">
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">آدرس</label></div>
            <textarea class="form-textarea" name="address"></textarea>
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">شماره تلفن</label></div>
            <input type="text" class="form-normal-input" name="phone_no">
        </div>

        <br>

        <div class="form-row">
            <input type="submit" name="submit" style="float: left" class="button success" value="ثبت اطلاعات">
        </div>

        <div class="form-row">
            <br>
        </div>
    </form>
</div>

<div>
    <br><br>
</div>

</body>
</html>