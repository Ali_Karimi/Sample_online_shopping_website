<?php

require_once ('../connection.php');

session_start();

$db = connect_to_database();

$get_stuffs_q = "SELECT pid, producer, name FROM product";
$stuffs_sql_array = $db->query($get_stuffs_q);

if (isset($_POST['submit'])) {

    $pid = $_POST["pid"];
    $start = $_POST["start_date"];
    $end = $_POST["end_date"];
    $amount = $_POST["amount"];

    $q = "INSERT INTO discount (pid, amount, start_time, end_time) VALUES ('$pid', '$amount', '$start', '$end')";
    $res = mysqli_query($db, $q);

}

disconnect_from_database($db);

?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>مدیریت تخفیفات</title>
    <link rel="stylesheet" type="text/css" href="../css/foundation.min.css">
    <link rel="stylesheet" type="text/css" href="../css/app.css">
    <link rel="stylesheet" type="text/css" href="../css/Forms.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>

<div class="header">
    <div class="topbar">
        <div class="topbar-right">
            <span>سلام، <?php echo $_SESSION['admin_email']; ?></span>
            <span><a href="Admin-login.php" class="button error">خروج</a></span>
        </div>
        <div class="topbar-left">
            <img class="logo" src="../img/logo.png">
        </div>
    </div>
    <div>
        <ul class="menu">
            <a href="Admin-employee.php"><li>مدیریت کارمندان</li></a>
            <a href="Admin-warehouse.php"><li>مدیریت انبارها</li></a>
            <a href="admin_product.php"><li>مدیریت کالاها</li></a>
            <a href="Admin-discount.html"><li>مدیریت تخفیف‌ها</li></a>

        </ul>
    </div>
</div>

<div class="container" >
    <h3 class="form-title">ایجاد تخفیف</h3>
    <hr style="color: #0a0a0a">
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">کالا</label></div>
            <select style="width: 260px" name="pid">
                <?php
                    if ($stuffs_sql_array->num_rows > 0) {
                        while ($row = $stuffs_sql_array->fetch_assoc())
                            echo '<option value="'.$row["pid"].'">'.$row["producer"].' - '.$row["name"].'</option>';
                    }
                ?>
            </select>
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">زمان شروع</label></div>
            <input type="date" class="form-normal-input" name="start_date">
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">زمان اتمام</label></div>
            <input type="date" class="form-normal-input" name="end_date">
        </div>
        <div class="form-row">
            <div class="form-labels-col"><label class="form-label">تخفیف</label></div>
            <input type="number" class="form-normal-input" name="amount">
            <span> تومان</span>
        </div>

        <br>

        <div class="form-row">
            <input type="submit" name="submit" class="button success" style="float: left" value="ثبت تخفیف">
        </div>

        <div class="form-row">
            <br>
        </div>
    </form>
</div>

<div>
    <br><br>
</div>

</body>
</html>