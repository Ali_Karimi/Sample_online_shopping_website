function adminlogin(val1, val2) {
    "use strict";
    var inputEmail = document.getElementById("inputEmail"), inputPassword = document.getElementById("inputPassword"), error = false;
    if (val1 !== "admin@admin.com" || val2 !== "admin") {
        inputEmail.style.borderColor = "crimson";
        inputPassword.style.borderColor = "crimson";
        error = true;
    }
    if (!error) {
        window.location.replace("Admin-discount.php");
    }
    
    return false;
}
    