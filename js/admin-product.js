
function typeChanged(val) {
    "use strict";
    if (val === "mobile") {
        $("#mobile-section").css("display", 'block');
        $("#laptop-section").css("display", 'none');
        $("#camera-section").css("display", 'none');
    } else if (val === "laptop") {
        $("#mobile-section").css("display", 'none');
        $("#laptop-section").css("display", 'block');
        $("#camera-section").css("display", 'none');
    } else {
        $("#mobile-section").css("display", 'none');
        $("#laptop-section").css("display", 'none');
        $("#camera-section").css("display", 'block');
    }
}