var password = document.getElementById("password");
var confirm_password = document.getElementById("confirm_password");
var valids = [{email: "user@user.com", pass: "password"}];

function containsObject(obj, list) {
    "use strict";
    var x, m;
    for (i = 0; i<list.length; i++)
        if (list[i].email === obj.email && list[i].pass === obj.pass) {
            return true;
        }
    }
    return false;
}

function validatePassword() {
    "use strict";
    if (password.value !== confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
        password.style.borderColor = "crimson";
        confirm_password.style.borderColor = "crimson";
    } else {
        confirm_password.setCustomValidity('');
        password.style.borderColor = "#ccc";
        confirm_password.style.borderColor = "#ccc";
    }
}
            
password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

function sendSignup(val) {
    "use strict";
    window.location.replace("index.html");
}

function sendLogin(email, pass) {
    "use strict";
    /*
    var tmp = {email: email, pass: pass};
    
    if (containsObject(tmp, valids)) {
        window.alert("has");
    } else {
        window.alert("!has");
    }*/
    /*document.write(containsObject(tmp, valids));*/
    window.location.replace("index.html");
}

