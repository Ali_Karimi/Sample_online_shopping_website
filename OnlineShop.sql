-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 28, 2016 at 04:38 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `OnlineShop`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf16_persian_ci NOT NULL,
  `password` varchar(30) COLLATE utf16_persian_ci NOT NULL,
  `role` varchar(20) COLLATE utf16_persian_ci NOT NULL DEFAULT 'admin'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf16 COLLATE=utf16_persian_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`, `role`) VALUES
(1, 'admin@admin.com', '123456', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE IF NOT EXISTS `color` (
`cid` int(11) NOT NULL,
  `cvalue` varchar(20) COLLATE utf16_persian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf16 COLLATE=utf16_persian_ci;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`cid`, `cvalue`) VALUES
(1, 'مشکی');

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE IF NOT EXISTS `discount` (
  `did` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_persian_ci;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`did`, `pid`, `amount`, `start_time`, `end_time`) VALUES
(0, 1, 100000, '2016-05-28 00:00:00', '2016-05-29 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `exist`
--

CREATE TABLE IF NOT EXISTS `exist` (
  `pid` int(11) NOT NULL,
  `wid` int(11) NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `include`
--

CREATE TABLE IF NOT EXISTS `include` (
  `oid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `pure_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laptop`
--

CREATE TABLE IF NOT EXISTS `laptop` (
  `lid` int(11) NOT NULL,
  `cpu` varchar(40) COLLATE utf16_persian_ci NOT NULL,
  `ram` varchar(40) COLLATE utf16_persian_ci NOT NULL,
  `display` varchar(40) COLLATE utf16_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `employee_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_send` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mobile`
--

CREATE TABLE IF NOT EXISTS `mobile` (
  `mid` int(11) NOT NULL,
  `gpu` varchar(30) COLLATE utf16_persian_ci NOT NULL,
  `display` varchar(30) COLLATE utf16_persian_ci NOT NULL,
  `cpu` varchar(30) COLLATE utf16_persian_ci NOT NULL,
  `ram` varchar(30) COLLATE utf16_persian_ci NOT NULL,
  `sim` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
`oid` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `deliver_cost` int(11) NOT NULL,
  `deliver_type` tinyint(4) NOT NULL,
  `status` varchar(20) COLLATE utf16_persian_ci DEFAULT NULL,
  `date` date NOT NULL,
  `person_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `national_code` int(10) NOT NULL,
  `fname` varchar(20) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `lname` varchar(20) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `gender` int(1) NOT NULL DEFAULT '0',
  `city` varchar(30) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `area` varchar(30) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `post_code` char(10) COLLATE utf16_persian_ci NOT NULL,
  `phone_no` char(11) COLLATE utf16_persian_ci DEFAULT NULL,
  `Eflag` int(1) NOT NULL DEFAULT '0',
  `salary` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `start_year` int(11) DEFAULT NULL,
  `role` varchar(15) COLLATE utf16_persian_ci DEFAULT 'seller',
  `wid` int(11) DEFAULT NULL,
  `Cflag` int(1) NOT NULL DEFAULT '0',
  `email` varchar(25) COLLATE utf16_persian_ci DEFAULT NULL,
  `password` varchar(20) COLLATE utf16_persian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_persian_ci;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`national_code`, `fname`, `lname`, `gender`, `city`, `area`, `address`, `post_code`, `phone_no`, `Eflag`, `salary`, `age`, `start_year`, `role`, `wid`, `Cflag`, `email`, `password`) VALUES
(0, 'علی', 'کریمی', 1, 'اصفهان', 'اصفهان', 'اصفهان', '۱۲۳۴۵۵۴۳۲۱', '۰۱۲۳۴۵۶۷۸۹۸', 1, 0, 0, 2016, 'deliver', NULL, 0, NULL, NULL),
(1234567890, 'ali', 'karimi', 1, 'esf', 'esf', 'esf', '1234567890', '03112345678', 1, 2000100, 21, 2016, 'warehouse', 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
`pid` int(11) NOT NULL,
  `producer` varchar(50) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_persian_ci,
  `price` int(11) NOT NULL,
  `image` varchar(200) COLLATE utf16_persian_ci DEFAULT NULL,
  `substance` varchar(20) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `add_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `mobile_id` int(11) DEFAULT NULL,
  `laptop_id` int(11) DEFAULT NULL,
  `category_type` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf16 COLLATE=utf16_persian_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`pid`, `producer`, `name`, `description`, `price`, `image`, `substance`, `add_date`, `mobile_id`, `laptop_id`, `category_type`) VALUES
(1, 'Apple', 'iphone', 'گوشی موبایل آیفون', 2000000, NULL, NULL, '2016-05-28 19:07:55', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_color`
--

CREATE TABLE IF NOT EXISTS `product_color` (
  `cid` int(11) NOT NULL,
  `pid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE IF NOT EXISTS `review` (
`rid` int(11) NOT NULL,
  `rate` tinyint(4) NOT NULL,
  `date` date NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_persian_ci,
  `like_count` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `pid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `warehouse`
--

CREATE TABLE IF NOT EXISTS `warehouse` (
`wid` int(11) NOT NULL,
  `name` varchar(25) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `capacity` int(11) NOT NULL,
  `city` varchar(25) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `area` varchar(30) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_persian_ci,
  `post_code` char(10) COLLATE utf16_persian_ci DEFAULT NULL,
  `phone_no` char(11) COLLATE utf16_persian_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf16 COLLATE=utf16_persian_ci;

--
-- Dumping data for table `warehouse`
--

INSERT INTO `warehouse` (`wid`, `name`, `capacity`, `city`, `area`, `address`, `post_code`, `phone_no`) VALUES
(1, 'انبار مرکزی', 2000, 'تهران', 'تهران', 'نارمک', '1234567890', '02112345678');

-- --------------------------------------------------------

--
-- Table structure for table `warranty`
--

CREATE TABLE IF NOT EXISTS `warranty` (
`war_id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `company` varchar(50) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `start_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_persian_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
 ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
 ADD PRIMARY KEY (`did`,`pid`);

--
-- Indexes for table `exist`
--
ALTER TABLE `exist`
 ADD PRIMARY KEY (`pid`,`wid`);

--
-- Indexes for table `include`
--
ALTER TABLE `include`
 ADD PRIMARY KEY (`oid`,`pid`);

--
-- Indexes for table `laptop`
--
ALTER TABLE `laptop`
 ADD PRIMARY KEY (`lid`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
 ADD PRIMARY KEY (`employee_id`,`user_id`);

--
-- Indexes for table `mobile`
--
ALTER TABLE `mobile`
 ADD PRIMARY KEY (`mid`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
 ADD PRIMARY KEY (`oid`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
 ADD PRIMARY KEY (`national_code`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
 ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `product_color`
--
ALTER TABLE `product_color`
 ADD PRIMARY KEY (`cid`,`pid`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
 ADD PRIMARY KEY (`rid`);

--
-- Indexes for table `warehouse`
--
ALTER TABLE `warehouse`
 ADD PRIMARY KEY (`wid`);

--
-- Indexes for table `warranty`
--
ALTER TABLE `warranty`
 ADD PRIMARY KEY (`war_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
MODIFY `oid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `warehouse`
--
ALTER TABLE `warehouse`
MODIFY `wid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `warranty`
--
ALTER TABLE `warranty`
MODIFY `war_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
